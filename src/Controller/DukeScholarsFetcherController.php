<?php

namespace Drupal\duke_scholars_fetcher\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Scholars@Duke Data Fetcher routes.
 */
class DukeScholarsFetcherController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
