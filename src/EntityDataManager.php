<?php

namespace Drupal\duke_scholars_fetcher;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\duke_scholars_entities\DukeScholarsEntityInterface;
use Drupal\duke_scholars_entities\Entity\DukeScholarsOrganization;
use Drupal\duke_scholars_entities\Entity\DukeScholarsPosition;
use Drupal\duke_scholars_entities\Entity\DukeScholarsProfile;
use Drupal\duke_scholars_fetcher\Fetcher;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * EntityDataManager service.
 */
class EntityDataManager
{

    /**
     * The file system service.
     *
     * @var FileSystemInterface
     */
    protected $fileSystem;

    /**
     * @var Fetcher
     */
    protected $fetcher;

    /**
     * @var EntityTypeManager
     */
    protected $entityTypeManager;

    /**
     * @var \Drupal\duke_scholars_entities\DukeScholarsEntityInterface
     */
    protected $entity;

    /**
     * @var QueueFactory
     */
    protected $queueFactory;

    /**
     * @var \Drupal\Core\Queue\QueueInterface
     */
    protected $importerQueue;

    /**
     * @var string
     */
    protected $entityType;

    /**
     * @var bool
     */
    protected $new;

    /**
     * @var string
     */
    protected $scholarsUri;

    /**
     * @var \Drupal\duke_scholars_entities\DukeScholarsEntityInterface
     */
    protected $orgParent;

    /**
     * Constructs an EntityDataManager object.
     *
     * @param \Drupal\Core\File\FileSystemInterface $file_system
     *   The file system service.
     * @param \Drupal\duke_scholars_fetcher\Fetcher $fetcher
     * @param \Drupal\Core\Queue\QueueFactory       $queue_factory
     */
    public function __construct(FileSystemInterface $file_system, Fetcher $fetcher, QueueFactory $queue_factory)
    {
        $this->fileSystem = $file_system;
        $this->fetcher = $fetcher;
        $this->queueFactory = $queue_factory;
    }

    public function checkIfEntityExists($URI = false, $label = '')
    {
        $uri = ($URI) ? $URI : $this->getUri();
        $database = \Drupal::database();
        $type = duke_scholars_fetcher_uri_type($uri);
        $table = ($type == 'people') ? 'duke_scholars_profile' : 'duke_scholars_organization';
        $sql = "SELECT `id` FROM {$table} WHERE `duke_scholars_uri` = :uri";
        $query = $database->query($sql, [':uri' => $uri]);
        $result =  $query->fetchField();
        if ($result) {
            $n = \Drupal::entityTypeManager()->getStorage($table)->load($result);
            $this->setEntity($n);
            $this->new = false;
        } else {
            $this->fetcher->prepareEndpoint($uri, false);
            if ($type == 'people') {
                $this->createNewEntity('duke_scholars_profile');
            } elseif ($type == 'organizations') {
                $this->createNewEntity('duke_scholars_organization', $label);
            }
        }
    }

    public function itemAlreadyMapped($uri, $isQueued = true)
    {
        $db = \Drupal::database();
        $q = $db->select('duke_scholars_fetcher');
        $q->condition('duke_scholars_uri', $uri, '=');
        if ($isQueued) {
            $q->condition('in_queue', '1', '=');
        }
        $num = $q->countQuery()->execute()->fetchCol();
        return (!empty($num[0]));
    }

    /**
     * Scholars@Duke URI
     *
     * @param $uri
     *
     * @param $eid
     * Drupal Entity ID
     *
     * @param int $inQueue
     * Is this item in the queue to be processed?
     */
    public function recordScholarsItem($uri, $eid, $inQueue = 0)
    {
        $lastUpdated = \Drupal::time()->getRequestTime();
        $db = \Drupal::database();
        $alreadyMapped = $this->itemAlreadyMapped($uri, $inQueue);
        if ($alreadyMapped) {
            $statement = $db->update('duke_scholars_fetcher');
            $statement->fields(
                [
                'last_updated' => $lastUpdated,
                'in_queue' => $inQueue
                ]
            );
            $statement->condition('duke_scholars_uri', $uri, '=');
        } else {
            $statement = $db->insert('duke_scholars_fetcher');
            $statement->fields(
                [
                'eid' => $eid,
                'duke_scholars_uri' => $uri,
                'last_updated' => $lastUpdated,
                'in_queue' => $inQueue]
            );
        }
        try {
            $statement->execute();
        } catch (\Exception $e) {
            \Drupal::logger('warning')->error($e->getMessage());
        }
    }

    public function updateNeeded($oldModifiedAt, $newModifiedAt)
    {
        $needed = true;
        if ($oldModifiedAt && !empty($oldModifiedAt) && $newModifiedAt) {
            $needed = (strtotime($oldModifiedAt) < (strtotime($newModifiedAt)));
        } elseif ($this->new) {
            $needed = $this->new;
        }
        return $needed;
    }

    public function fetch($endpointsObject = [], $section = false, $keys = [], $subOrgs = false)
    {
        $returnValue = false;

        $uri = $this->getUri();
        if ($uri) {
            if (empty($endpointsObject) || $subOrgs) {
                $this->fetcher->prepareEndpoint($uri, $subOrgs);
                $returnValue = $this->fetcher->fetch($uri);
            }
            if ($section && $keys && !empty($endpointsObject)) {
                $returnValue = $this->fetcher->fetchIndividualEndpointsData($endpointsObject, $section, $keys);
            }
        } else {
            $message = 'Failed to fetch data, no URI set';
            \Drupal::logger('duke_scholars_fetcher')->error($message);
        }
        return $returnValue;
    }

    public function clearData()
    {
        $this->scholarsUri = null;
        $this->orgParent = null;
        $this->entity = null;
        $this->new = null;
        $this->entityType = null;
    }

    public function setEntityProperties()
    {
        switch ($this->getEntityType()) {
        case 'duke_scholars_profile':
            $this->setProfileProperties();
            break;
        case 'duke_scholars_organization':
            $this->setOrganizationProperties();
            break;
        }
        return true;
    }

    public function setProfileProperties()
    {
        $responseData = $this->fetch([], false, [], false);
        if ($responseData) {
            $active = FALSE;
            $active = $responseData->data->active;

            // Unpublish the scholar profile if active status is false (404)
            if (!$active) {
                $this->entity->setUnpublished();
                $this->entity->setActive($active);
                $this->saveEntity();
                \Drupal::logger('duke_scholars_fetcher')->notice('Unpublish Inactive Scholar URI: '.$this->scholarsUri);
            } else {
                $this->entity->setActive($active);

                $this->entity->setLastModified($responseData->data->updatedAt);
                // fetch different datasets
                $overviewData = $this->fetch(
                  $responseData, 'overview', [
                    'overview',
                    'imageFileName',
                    'imageThumbnailUri',
                    'imageUri',
                    'firstName',
                    'middleName',
                    'lastName',
                    'phoneNumber',
                    'primaryEmail',
                    'preferredTitle']
                );
                $addressesData = $this->fetch(
                  $responseData, 'addresses', [
                    'vivoType',
                    'label'
                  ]
                );
                $positionsData = $this->fetch(
                  $responseData, 'positions', [
                    'label',
                    'uri',
                    'vivoType',
                    'attributes'
                  ]
                );

                $educationsData = $this->fetch($responseData, 'educations', ['attributes']);
                $vivoTypeTerm = $this->vivoTypeTerm($responseData->data->vivoType);
                $imageUrls['thumb'] = $overviewData['imageThumbnailUri'][0];
                $imageUrls['image'] = $overviewData['imageUri'][0];
                $preferredTitle = $overviewData['preferredTitle'][0];
                $positions = $this->processPositions($positionsData, $preferredTitle);

                // set values on the entity
                $first = $overviewData['firstName'][0];
                $middle = $overviewData['middleName'][0];
                $last = $overviewData['lastName'][0];

                $this->entity->setTitle(implode(" ", [$first, $middle, $last]));
                $this->entity->duke_scholars_vivotype->setValue(['target_id' => $vivoTypeTerm->id()]);
                if ($imageUrls) {
                  $this->entity->set(
                    'duke_scholars_profile_thumbnail',
                    [
                      'thumbnail' => $imageUrls['thumb'],
                      'portrait' => $imageUrls['image'],
                      'alt_text' => $this->entity->label()
                    ]
                  );
                }
                $this->entity->set('duke_scholars_profile_name_first', $first);
                $this->entity->set('duke_scholars_profile_name_middle', $middle);
                $this->entity->set('duke_scholars_profile_name_last', $last);
                if ($overviewData['overview'][0]) {
                  $this->entity->set('duke_scholars_profile_overview', $overviewData['overview'][0]);
                }
                else {
                  $this->entity->set('duke_scholars_profile_overview', '');
                }

                if ($overviewData['phoneNumber'][0]) {
                  $this->entity->set('duke_scholars_profile_phone', $overviewData['phoneNumber'][0]);
                }
                else {
                  $this->entity->set('duke_scholars_profile_phone', '');
                }

                if ($overviewData['primaryEmail'][0]) {
                  $this->entity->set('duke_scholars_profile_email', $overviewData['primaryEmail'][0]);
                }
                else {
                  $this->entity->set('duke_scholars_profile_email', '');
                }

                if ($addressesData && count($addressesData['label'])>0) {
                  for ($i = 0; $i < 2; $i++) {
                    if (isset($addressesData['label'][$i])) {
                      if (strpos($addressesData['vivoType'][$i], 'Location')) {
                        $this->entity->set('duke_scholars_profile_address_1', $addressesData['label'][$i]);
                      }
                      else {
                        $this->entity->set('duke_scholars_profile_address_2', $addressesData['label'][$i]);
                      }
                    }
                  }
                };

                $this->entity->duke_scholars_profile_positions->setValue([]);
                foreach ($positions as $position) {
                  $this->entity->duke_scholars_profile_positions->appendItem($position);
                }
                $this->entity->duke_scholars_profile_preferred_title->setValue($preferredTitle);
                $this->saveEntity();
            }
        } else {
            $message = "Failed to fetch data for $this->scholarsUri";
            \Drupal::logger('duke_scholars_fetcher')->error($message);
        }
    }

    /**
     * Parse the positions data object and create entities for each position
     * these include the position itself (entity), the organization to which it
     * belongs (entity) and the vivotype it's tagged with
     *
     * @param object $positionsData
     *
     * @param $preferredTitle
     *
     * @return array
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
    public function processPositions($positionsData, $preferredTitle)
    {
        $positions = [];
        if (isset($positionsData) && !empty($positionsData)) {
            $length = count($positionsData['label']);
            for ($i = 0; $i <= $length -1; $i++) {

                $org = $this->organizationEntity($positionsData['attributes'][$i]->organizationUri, $positionsData['attributes'][$i]->organizationLabel);
                $vivoTypeTerm = $this->vivoTypeTerm($positionsData['vivoType'][$i]);
                $position = $this->positionEntity(
                    $positionsData['uri'][$i],
                    $positionsData['label'][$i],
                    $org->id(),
                    $vivoTypeTerm->id(),
                    $preferredTitle
                );
                $positions[] = [
                'target_id' => $position->id()
                ];
            }
        }

        return $positions;
    }

    /**
     * @param $uri
     * @param $label
     * @param $orgID
     * @param $tid
     * @param $preferredTitle
     *
     * @return bool|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|null
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
    public function positionEntity($uri, $label, $orgID, $tid, $preferredTitle)
    {
        $position = false;
        if ($uri) {
            $database = \Drupal::database();
            $sql = 'SELECT id FROM {duke_scholars_position} WHERE position_uri = :uri';
            $query = $database->query($sql, [':uri' => $uri]);
            $result = $query->fetchField();
            $isPreferred = ($preferredTitle == $label);
            if ($result) {
                /**
                * @var DukeScholarsPosition $position
                */
                $position = \Drupal::entityTypeManager()->getStorage('duke_scholars_position')->load($result);
                if ($position->getTitle() != $label) {
                    $position->setTitle($label);
                    \Drupal::logger('duke_scholars_fetcher')->notice('Position title updated.');
                }

                $position->setPreferred($isPreferred);
                $position->save();
            } else {
                $position = DukeScholarsPosition::create(
                    [
                    'title' => $label,
                    'position_uri' => $uri,
                    'parent_org' => $orgID,
                    'position_vivo_type' => $tid,
                    'position_preferred' => ($isPreferred) ? 1 : 0
                    ]
                );
                try {
                    $position->save();
                } catch (EntityStorageException $e) {
                    \Drupal::logger('warning')->error($e->getMessage());
                }
            }
        }
        return $position;
    }

    /**
     * @param $uri
     * @param $orgLabel
     *
     * @return bool|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|null
     */
    public function organizationEntity($uri, $orgLabel)
    {
        $org = false;
        if ($uri) {
            $database = \Drupal::database();
            $sql = 'SELECT id FROM {duke_scholars_organization} WHERE duke_scholars_uri = :uri';
            $query = $database->query($sql, [':uri' => $uri]);
            $result =  $query->fetchField();
            if ($result) {
                $org = \Drupal::entityTypeManager()->getStorage('duke_scholars_organization')->load($result);
            } else {
                $org = DukeScholarsOrganization::create(
                    [
                    'title' => $orgLabel,
                    'duke_scholars_uri' => ['uri' => $uri]
                    ]
                );
                try {
                    $org->save();
                } catch (EntityStorageException $e) {
                    \Drupal::logger('warning')->error($e->getMessage());
                }
            }
            $this->recordScholarsItem($uri, $org->id(), 0);
        }
        return $org;
    }

    /**
     * @param $label
     *
     * @return bool|\Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|mixed
     * @throws \Drupal\Core\Entity\EntityStorageException
     */
     public function vivoTypeTerm($label) {
       $term = false;

       if ($label) {
         // Get the Entity Type Manager service.
         $entityTypeManager = \Drupal::entityTypeManager();

         // Replace 'duke_scholars_vivotypes' with your vocabulary machine name.
         $vid = 'duke_scholars_vivotypes';

         // Load the vocabulary.
         $vocabulary = $entityTypeManager->getStorage('taxonomy_vocabulary')->load($vid);

         if ($vocabulary instanceof EntityInterface) {
           // Load the term by name.
           $termStorage = $entityTypeManager->getStorage('taxonomy_term');
           $terms = $termStorage->loadByProperties([
             'vid' => $vocabulary->id(),
             'name' => $label,
           ]);

           if (!empty($terms)) {
             // If a term with the given name exists, use the first one.
             $term = reset($terms);
           } else {
             // Create a new term if it doesn't exist.
             $term = Term::create([
               'name' => $label,
               'vid' => $vid,
             ]);

             try {
               $term->save();
             } catch (EntityStorageException $e) {
               \Drupal::logger('warning')->error($e->getMessage());
             }
           }
         } else {
           \Drupal::logger('warning')->error('Vocabulary ' . $vid . ' not found.');
         }
       }

       return $term;
     }

    public function setOrganizationProperties()
    {
        $setToUpdate = $this->entity->get('import_associated_people')->getString();
        if ($setToUpdate) {
            $responseData = $this->fetch();
            $this->importerQueue = $this->queueFactory->get('duke_scholars_fetcher');
            if ($responseData) {
                $this->entity->duke_scholars_organization_person_uris->setValue([]);
                $uris = $this->parseOrgForPersonURIS($responseData);
                foreach ($uris as $uri) {
                    $this->entity->duke_scholars_organization_person_uris->appendItem($uri);
                    if (!$this->itemAlreadyMapped($uri, false)) {
                        $item = new \stdClass();
                        $item->uri = $uri;
                        $item->orgNid = $this->entity->id();
                        $item->type = 'people';
                        $this->importerQueue->createItem($item);
                    }
                }
            }
            $subOrgData = $this->fetch([], false, [], true);
            if ($subOrgData) {
                $this->parseSubOrg($subOrgData);
            }
        }
        if ($this->getOrgParentEntity()) {
            $this->entity->parent_org->appendItem(['target_id' => $this->getOrgParentEntity()->id()]);
        }
        $this->saveEntity();
    }

    public function saveEntity()
    {
        try {
            $this->entity->save();
        } catch (EntityStorageException $e) {
            watchdog_exception('duke_scholars_fetcher', $e, $e->getMessage());
        }
    }

    public function parseSubOrg($org, $parent = false)
    {
        if (is_array($org->subOrgs)) {
            array_walk(
                $org->subOrgs, function ($item, $key, $parent) {
                    if (!$this->itemAlreadyMapped($item->URI, false)) {
                        $i = new \stdClass();
                        $i->uri = $item->URI;
                        $i->type = 'org';
                        $i->label = $item->name;
                        $i->orgParentURI = $parent->URI;
                        $i->orgParentName = $parent->name;
                        $this->importerQueue->createItem($i);
                    }
                    if (is_array($item->subOrgs) && !empty($item->subOrgs)) {
                        $this->parseSubOrg($item, $parent);
                    }
                }, $org
            );
        }
    }

    public function parseOrgForPersonURIS($responseData)
    {
        $uris = [];
        foreach ($responseData as $datum) {
            $uris[] = $datum->uri;
        }
        return array_unique($uris);
    }

    public function createNewEntity($type, $label = 'stub')
    {
        /**
   * @var DukeScholarsEntityInterface $entity
*/
        $entity = null;
        switch ($type) {
        case 'duke_scholars_profile':
            $entity = DukeScholarsProfile::create(
                [
                'title' => $label,
                'duke_scholars_uri' => ['uri' => $this->getUri()]
                ]
            );
            break;
        case 'duke_scholars_organization':
            $entity = DukeScholarsOrganization::create(
                [
                'title' => $label,
                'duke_scholars_uri' => ['uri' => $this->getUri()]
                ]
            );
            break;
        }
        $this->setEntity($entity);
        $this->new = true;
    }

    public function getEntityType()
    {
        return $this->entityType;
    }

    public function setEntity($entity)
    {
        $type = $entity->getEntityTypeId();
        if (in_array(
            $type, [
            'duke_scholars_profile',
            'duke_scholars_organization'
            ]
        )
        ) {
            $this->entityType = $type;
            $this->setUri($entity->getScholarsUri());
            $this->entity = $entity;
        }
    }

    public function setOrgParentEntity(DukeScholarsEntityInterface $entity)
    {
        $this->orgParent = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getOrgParentEntity()
    {
        return $this->orgParent;
    }

    public function setUri($uri)
    {
        $this->scholarsUri = $uri;
    }

    private function getUri()
    {
        return $this->scholarsUri;
    }

    public function isNew()
    {
        return $this->new;
    }

}
