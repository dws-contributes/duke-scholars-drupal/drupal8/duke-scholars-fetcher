<?php

namespace Drupal\duke_scholars_fetcher;

use Drupal\Core\File\FileSystemInterface;
use GuzzleHttp\ClientInterface;

/**
 * Fetcher service.
 */
class Fetcher {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface $httpClient
   */
  protected $httpClient;

  /**
   * The active configuration storage.
   *
   * @var \Drupal\Core\Config\StorageInterface $storage
   */
  protected $storage;

  /**
   * @var string $endpointFile
   */
  protected $endpointFile;

  /**
   * @var string $endpointUri
   */
  protected $endpointUri;

  /**
   * @var object|NULL $responseData
   */
  protected $responseData;

  /**
   * Constructs a Fetcher object.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(FileSystemInterface $file_system, ClientInterface $http_client) {
    $this->fileSystem = $file_system;
    $this->httpClient = $http_client;
  }

  public function fetch() {

    try {
      // Make the request with http_errors set to false
      $res = $this->httpClient->request('GET', $this->endpointUri, ['http_errors' => false]);

      // Get the status code of the response
      $statusCode = $res->getStatusCode();

      // If 200, profile is active, set body data from response and return
      if ($statusCode == 200) {
        $body = $res->getBody();

        return json_decode($body);

      //If 404, profile has been deactivated, set essential body data for active false for unpublishing in EntityDataManager->setProfileProperties
      } elseif ($statusCode == 404) {

        // Barebones/essential body data required to continue with unpublishing
        $body = new \stdClass();
        $body->data = new \stdClass();
        $body->data->active = 0;

        return $body;

      } else {
        // Handle other HTTP status codes
        throw new \Exception("Unhandled HTTP status code: $statusCode");
      }
    } catch (\GuzzleHttp\Exception\GuzzleException $e) {
      // Consider logging the error or rethrowing a more specific exception
      throw new \Exception("An error occurred during the HTTP request: " . $e->getMessage());
    }

  }

  public function prepareEndpoint($uri, $subOrgs = FALSE) {
    $uriParts = explode('/', $uri);
    $bareUri = end($uriParts);
    $uriType = duke_scholars_fetcher_uri_type($uri);
    $this->endpointUri = NULL;
    if ($uriType) {
      if ($uriType == 'people') {
        $type = 'endpoints.json';
      } elseif ($uriType == 'organizations') {
        $type = 'people/all.json';
        if ($subOrgs) {
          $this->endpointUri = 'https://scholars.duke.edu/orgservice?getTree=1&uri=' . $uri;
          $type = 'orgservice.json';
        }
      }
      $api_endpoint = \Drupal::config('duke_scholars_fetcher.settings')->get('duke_scholars_fetcher_widget_base_url');
      if (!$this->endpointUri) {
        $this->endpointUri = $api_endpoint . '/' . $uriType . '/' . $type . '?uri=' . $uri;
      }
      $this->endpointFile = $bareUri . '-' . str_replace('/', '_', $type);
    }
  }

  public function fetchIndividualEndpointsData($endpointsObject, $section, $keys = array()) {
    $returnObject = NULL;
    if ($endpointsObject) {
      if (property_exists($endpointsObject->data->sections, $section)) {
        $sectionData = $this->httpClient->request('GET', $endpointsObject->data->sections->$section, [])->getBody();
        if ($sectionData) {
          $body = json_decode($sectionData);
          foreach ($keys as $key) {
            foreach ($body as $item) {
              $returnObject[$key][] = (property_exists($item, $key)) ? $item->$key : false;
            }
          }
        }
      }
    }
    return $returnObject;
  }
}
