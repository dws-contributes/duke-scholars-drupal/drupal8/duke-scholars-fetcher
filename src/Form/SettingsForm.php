<?php

namespace Drupal\duke_scholars_fetcher\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Scholars@Duke Data Fetcher settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  private $fileSystem;

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   */
  public function __construct(ConfigFactoryInterface $config_factory, FileSystemInterface $fileSystem) {
    parent::__construct($config_factory);
    $this->fileSystem = $fileSystem;
  }

  /**
   * @param \Psr\Container\ContainerInterface $container
   *
   * @return \Drupal\Core\Form\ConfigFormBase|\Drupal\duke_scholars_fetcher\Form\SettingsForm|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('file_system')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'duke_scholars_fetcher_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['duke_scholars_fetcher'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\ImmutableConfig $config */
    $config = $this->config('duke_scholars_fetcher.settings');
    $form['duke_scholars_fetcher_widget_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scholars@Duke API base url, e.g. https://scholars.duke.edu/widgets/api/v0.9'),
      '#default_value' => ($config->get('duke_scholars_fetcher_widget_base_url')) ? $config->get('duke_scholars_fetcher_widget_base_url') : 'https://scholars.duke.edu/widgets/api/v0.9',
    ];

    $form['duke_scholars_fetcher_cache_max_age'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose Max Age'),
      '#options' => [
        '1 day' => $this->t('One Day'),
        '1 week' => $this->t('One Week'),
        '1 month' => $this->t('One Month')
      ],
      '#default_value' => ($config->get('duke_scholars_fetcher_cache_max_age')) ? $config->get('duke_scholars_fetcher_cache_max_age') : NULL,
    ];

    $form['duke_scholars_fetcher_disable_importer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Importer'),
      '#default_value' => ($config->get('duke_scholars_fetcher_disable_importer')) ? $config->get('duke_scholars_fetcher_disable_importer') : NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
//    if ($form_state->getValue('example') != 'example') {
//      $form_state->setErrorByName('example', $this->t('The value is not correct.'));
//    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $jsonCacheDir = $form_state->getValue('duke_scholars_fetcher_json_cache_directory') ?? '';
    try {
      $this->fileSystem->prepareDirectory($jsonCacheDir, FileSystemInterface::CREATE_DIRECTORY);
    }
    catch (\Exception $exception) {
      MessengerInterface::addMessage('could not create directory', MessengerInterface::TYPE_ERROR);
    }

    $config = $this->configFactory()->getEditable('duke_scholars_fetcher.settings');
    $config->set('duke_scholars_fetcher_json_cache_directory', $form_state->getValue('duke_scholars_fetcher_json_cache_directory'));
    $config->set('duke_scholars_fetcher_widget_base_url', $form_state->getValue('duke_scholars_fetcher_widget_base_url'));
    $config->set('duke_scholars_fetcher_cache_max_age', $form_state->getValue('duke_scholars_fetcher_cache_max_age'));
    $config->set('duke_scholars_fetcher_disable_importer', $form_state->getValue('duke_scholars_fetcher_disable_importer'));
    $config->save();

    // Check if Disable importer is checked, if so, clear import queue
    if ($form_state->getValue('duke_scholars_fetcher_disable_importer')) {
      $queue_factory = \Drupal::service('queue');
      $queue = $queue_factory->get('duke_scholars_fetcher');
      $queue->deleteQueue();

      $messenger = \Drupal::messenger();
      $messenger->addMessage('Import Queue has been cleared.');
      \Drupal::logger('duke_scholars_fetcher')->notice('Scholars Import Queue has been cleared.');

      // Set all scholars as not queued
      $db = \Drupal::database();
      $sql = 'UPDATE {duke_scholars_fetcher} SET in_queue = 0';
      $db->query($sql);
    }

    parent::submitForm($form, $form_state);
  }
}
