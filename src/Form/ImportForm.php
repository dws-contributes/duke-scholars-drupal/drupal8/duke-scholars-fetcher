<?php

namespace Drupal\duke_scholars_fetcher\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImportForm extends FormBase {

  use MessengerTrait;

  /** @var \Drupal\Core\Queue\QueueFactory $queueFactory */
  protected $queueFactory;

  /** @var \Drupal\Core\Queue\QueueWorkerManagerInterface $queueManager */
  protected $queueManager;

  public function __construct(QueueFactory $queueFactory, QueueWorkerManagerInterface $queueManager) {
    $this->queueFactory = $queueFactory;
    $this->queueManager = $queueManager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \ImportForm|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker')
    );
  }

  /**
   * @return string
   */
  public function getFormId() {
    return 'duke_scholars_fetcher_import';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $queue = $this->queueFactory->get('duke_scholars_fetcher');

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Import @number Scholars@Duke profiles', ['@number' => $queue->numberOfItems()]),
    ];

    $form['queue_all_items'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Queue all items for import'),
      '#description' => $this->t('Check to re-import all Scholars@Duke profiles.')

    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Scholars'),
      '#button_type' => 'primary',
      '#submit' => ['::submitForm']
    ];

    $form['actions']['clear_queue'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Import Queue'),
      '#button_type' => 'primary',
      '#submit' => ['::submitClearQueue']
    ];

    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitClearQueue(array &$form, FormStateInterface $form_state) {
    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('duke_scholars_fetcher');
    $queue->deleteQueue();

    $messenger = \Drupal::messenger();
    $messenger->addMessage('Import Queue has been cleared.');
    \Drupal::logger('duke_scholars_fetcher')->notice('Scholars Import Queue has been cleared.');

    // Set all scholars as not queued
    $db = \Drupal::database();
    $sql = 'UPDATE {duke_scholars_fetcher} SET in_queue = 0';
    $db->query($sql);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $importDisabled = \Drupal::config('duke_scholars_fetcher.settings')->get('duke_scholars_fetcher_disable_importer');

    if ($form_state->getValue('queue_all_items') && $importDisabled) {
      /** "Queue all items for import" is checked and import is disabled **/
      // Display Message that import is disabled
      $messenger = \Drupal::messenger();
      $messenger->addWarning('Can not queue items for import. Import is disabled in Fetcher Settings.');

    } else if ($form_state->getValue('queue_all_items') && !$importDisabled) {
      /** "Queue all items for import" is checked and import is not disabled **/
      $queue_factory = \Drupal::service('queue');
      $queue = $queue_factory->get('duke_scholars_fetcher');
      $db = \Drupal::database();
      $sql = 'SELECT duke_scholars_uri, eid FROM {duke_scholars_fetcher} WHERE in_queue = 0';
      $query = $db->query($sql);
      $results = $query->fetchAll();
      foreach ($results as $result) {
        $type = duke_scholars_fetcher_uri_type($result->duke_scholars_uri);
        if ($type) {
          $item = new \stdClass();
          $item->uri = $result->duke_scholars_uri;
          $item->eid = $result->eid;
          $item->type = $type;
          $queue->createItem($item);
          $db->update('duke_scholars_fetcher')
            ->fields(['in_queue' => 1])
            ->condition('duke_scholars_uri', $result->duke_scholars_uri, '=')
            ->execute();
        }
      }

    } else {

      /** Import Scholars and "Queue all items for import" is not checked **/
      // First check if Import Disabled
      if (!$importDisabled) {
        $queue          = $this->queueFactory->get( 'duke_scholars_fetcher' );
        $operations     = [];
        $num_operations = $queue->numberOfItems();

        for ( $i = 0; $i < $num_operations; $i ++ ) {
          $operations[] = [
            'Drupal\duke_scholars_fetcher\Form\ImportForm::processBatchItem',
            [
              $i + 1,
              $this->t( '(processing @operation)', [ '@operation' => $i ] )
            ],
          ];
        }

        $batch = [
          'title'      => $this->t( 'Processing all Scholars@Duke imports' ),
          'operations' => $operations,
          'finished'   => 'Drupal\duke_scholars_fetcher\Form\ImportForm::batchFinished',
        ];
        batch_set( $batch );
      } else {
        // Display Message that import is disabled
        $messenger = \Drupal::messenger();
        $messenger->addWarning('Can not import Scholars. Import is disabled in Fetcher Settings.');
      }

    }
  }

  public static function processBatchItem($id, $operation_details, &$context) {

    $queue_factory = \Drupal::service('queue');
    $queue_manager = \Drupal::service('plugin.manager.queue_worker');
    $queue = $queue_factory->get('duke_scholars_fetcher');
    $queue_worker = $queue_manager->createInstance('duke_scholars_fetcher');

    if ($item = $queue->claimItem()) {

      try {
        $context['results'][] = $item->data->uri . ' ' . $operation_details;
        $context['message'] = t('Importing scholar with uri "@uri" @details',
          [
            '@uri' => $item->data->uri,
            '@details' => $operation_details,
          ]
        );

        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      } catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
      }
    }
  }

  public static function batchFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage('All Items successfully imported');
      $messenger->addMessage(t('@count items were imported.',
        [
          '@count' => count($results),
        ]
      ));
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage('an error occurred while importing data. @operation failed with arguments : @args',
        [
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        ]
      );
    }
  }
}
