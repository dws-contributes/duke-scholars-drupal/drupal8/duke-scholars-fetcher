<?php

namespace Drupal\duke_scholars_fetcher\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "duke_scholars_fetcher_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Scholars@Duke Data Fetcher")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
