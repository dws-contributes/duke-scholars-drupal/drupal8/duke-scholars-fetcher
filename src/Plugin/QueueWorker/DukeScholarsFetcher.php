<?php

namespace Drupal\duke_scholars_fetcher\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\duke_scholars_fetcher\EntityDataManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'duke_scholars_fetcher_queue' queue worker.
 */
class DukeScholarsFetcher extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /** @var EntityDataManager $datamanager */
  private $datamanager;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var EntityDataManager $datamanager */
    $datamanager = $container->get('duke_scholars_fetcher.entity_data_manager');
    return new static($configuration, $plugin_id, $plugin_definition, $datamanager);
  }

  public function __construct(array $configuration, $plugin_id, $plugin_definition, $datamanager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->datamanager = $datamanager;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    $uri = isset($data->uri) && $data->uri ? $data->uri : NULL;
    $importDisabled = \Drupal::config('duke_scholars_fetcher.settings')->get('duke_scholars_fetcher_disable_importer');

    if (!$uri) {
      throw new \Exception('Missing URI');
      return;
    }

    if ($importDisabled) {
      $message = "Do not process item. Import is disabled in Fetcher Settings.";
      \Drupal::logger('duke_scholars_fetcher')->notice($message);
      return;
    }

    if ($data->type == 'org' && $data->orgParentURI) {
      $this->datamanager->checkIfEntityExists($data->orgParentURI, $data->orgParentName);
      $this->datamanager->setOrgParentEntity($this->datamanager->getEntity());
      if ($this->datamanager->getOrgParentEntity()) {
        $this->datamanager->getOrgParentEntity()->setScholarsUri($uri);
      }
      if ($this->datamanager->isNew()) {
        $this->datamanager->saveEntity();
      }
      $this->datamanager->clearData();
    }

    $this->datamanager->setUri($uri);

    $this->datamanager->checkIfEntityExists();
    if (isset($data->label) && $data->label) {
      $this->datamanager->getEntity()->setTitle($data->label);
    }
    $this->datamanager->setEntityProperties();

    $eid = $this->datamanager->getEntity()->id();

    if ($eid) {
      $this->datamanager->recordScholarsItem($uri, $eid, 0);
      $message = "Imported $data->uri to the entity $eid";
      \Drupal::logger('duke_scholars_fetcher')->notice($message);
      $this->datamanager->clearData();
    } else {
      $message = "Failed to import $data->uri";
      throw new \Exception($message);
    }
  }
}
