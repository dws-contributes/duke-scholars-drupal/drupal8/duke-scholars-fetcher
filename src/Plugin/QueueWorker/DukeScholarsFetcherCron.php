<?php

namespace Drupal\duke_scholars_fetcher\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Defines 'duke_scholars_fetcher_queue' queue worker.
 *
 * @QueueWorker(
 *   id = "duke_scholars_fetcher",
 *   title = @Translation("Duke Scholars Fetcher Queue"),
 *   cron = {"time" = 10}
 * )
 */

class DukeScholarsFetcherCron extends DukeScholarsFetcher {}
